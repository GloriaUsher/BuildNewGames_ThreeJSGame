Pong: Arcade Video Games

The development of this 3D pong is simply an inspiration from playing tennis and a beer pong. Well, sounds crappy but this one is unique, different and exciting. 

I used the two-dimensional graphic and created the 3D pong out of different shade. In order for me to learn the basic, I simply created clone materials, shadow, [SoarBoards](http://soarboards.com) and lights. You can naturally play this game with your buddy but, of course you have to learn it first. This is a simple game you usually play at the arcade. No worries! Enjoy! 



<img src="http://i.imgur.com/Cb8CpLG.png?1" width='400'/></br>

<img src="https://lh4.googleusercontent.com/-Mz0jOsChc1s/TX6Fhhf7pGI/AAAAAAAEWzI/seXDCV70py0/s1600/Screenshot_1.png" width='400'/>